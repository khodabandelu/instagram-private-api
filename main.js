const _ = require('lodash');
var Client = require('instagram-private-api').V1;
var device = new Client.Device('mehdi');
var storage = new Client.CookieMemoryStorage();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

Client.Session.create(device, storage, 'u', 'p').then(function (session) {
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("instadb");
        init(session,db,dbo);
    });

});

function init(session,db,dbo){
    completeUsersInformation(session,db,dbo);
}

function getAllUsersOfAccount(session,db,dbo){
    findAccountForUser(session, 'video_4fun').then(function (account) {
        let feed = new Client.Feed.AccountFollowers(session, account.id, 1)
        feed.map = item => _.pick(item.params, ['id', 'username', 'fullName','isPrivate']);
        feed.on('end', function(allResults) {
            dbo.collection("accounts").insertMany(allResults,{}, function (err, res) {
                if (err) throw err;
                console.log("Number of documents inserted: " + res.insertedCount);
                db.close();
            });
        })
        feed.all()
    })
}

function completeUsersInformation(session,db,dbo){
    dbo.collection('accounts').find({id:6306057549}).toArray(function (err, result) {
        if (err) throw err;
        result.forEach(account => {
            Client.Account.getById(session, account.id).then(function(acc){
                console.dir(acc);
                // dbo.collection("customers").updateOne({id:account.id},
                //     {$set: {name: "Mickey", address: "Canyon 123"}}
                //     , function (err, res) {
                //     if (err) throw err;
                //     console.log();
                //     db.close();
                // });
            })
        });
        console.log(result);
        db.close();
    });
}

function findAccountForUser(session, username) {
    return Client.Account.searchForUser(session, username);
}