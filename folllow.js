const _ = require('lodash');
var Client = require('instagram-private-api').V1;
var device = new Client.Device('mehdi');
var storage = new Client.CookieMemoryStorage();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var async = require("async");
var argv = require('minimist')(process.argv.slice(2));
var users = require('./data/json/users.json');
var pages = require('./data/json/pages.json');

var throttledQueue = require('throttled-queue');
var throttle = throttledQueue(1,90*1000 ); // at most make 1 request every second.
var throttleUn = throttledQueue(1,10*1000 ); // at most make 1 request every second.
var throttleUnForHours = throttledQueue(1,6*60*60*1000 ); // at most make 1 request every second.
init(argv.m,argv.u,argv.p,argv.c);

async function init(method,user,page,count) {
    var u=1;
    if(user!==undefined){
        u=user;
    }
    var p=0;
    if(page!==undefined){
        p=page;
    }
    var c=200;
    if(count!==undefined){
        c=count;
    }
    var m="follow";
    if(method!==undefined){
        m=method;
    }

    var us = await getConnection(u).catch(error=>{console.log('session caught:',error)});
    var session = us.session;
    var dbo = us.dbo;

    if(m=="follow"){
        console.log("start follow "+new Date())
        follow(session,dbo,u,p,c);
    }else if(m=="unfollow") {
        // for (var x = 0; x < 1000; x++) {
            // throttleUnForHours(function () {
                console.log("start unfollow "+new Date())
                unfollow(session, dbo, u, p, c);
            // });
        // }
    }

}
async function getConnection(user) {
    var session = await Client.Session.create(device, storage,  users[user].username, users[user].password);
    const db = await MongoClient.connect(url).catch(error=>{console.log('mongo caught:',error.messaage)});
    const dbo = db.db("instadb");
    return {
        session: session,
        dbo: dbo
    }
}

async function follow(session,dbo,user,page,count){
    const MyCollection = dbo.collection('account');
    const result = await MyCollection.find({
        followingCount: {$gt: 20, $lt: 1000},
        followerCount: {$lt: 5000, $gt: 100},
        $or:[{followRequest:{$exists:false}},{"followRequest.user":{$ne:users[user].id}}]
    }).sort({_id:-1}).toArray().catch(error=>{console.log('find caught:',error.messaage)});

    console.log(result.length);
    var index = 0;
    for (let acc of result) {
        throttle(function() {
            Client.Relationship.create(session, acc._id).then(function (data) {
                var now = new Date();
                var newvalues = {$push: {followRequest: {reqFollow: true, user: users[user].id,followDate:now}}};
                MyCollection.updateOne({_id: acc._id}, newvalues, function (err, res) {
                    if (err) throw err;
                });
                console.log(index+" follow success in "+now+" :"+acc._id);
            }).catch(error=>{
                var now = new Date();
                var newvalues = {$push: {followRequest: {reqFollow: false, user: users[user].id,followDate:now}}};
                MyCollection.updateOne({_id: acc._id}, newvalues, function (err, res) {
                    if (err) throw err;
                });
                console.log(index + ' follow caught in '+now+" :"+acc._id+"  with error: "+error)
            });
            index++;
        });
    }
}

async function unfollow(session,dbo,user,page,count){
    let feed = await new Client.Feed.AccountFollowing(session, users[user].id,count);
    var mediaArray = await feed.get();
    console.dir("following Count in seed: "+mediaArray.length);
    await getFeedsBody(session,dbo,mediaArray,user,count);
    // var medisArray = await getFeeds(session,dbo,feed,page,count);
}

async function getFeedsBody(session,dbo,allResults ,user,count) {
    //todo add date to condition for example 2 days
    var followed = await dbo.collection("account").find({
        $and:[{"followRequest.user":users[user].id},
            {"followRequest.reqUnfollow":{$exists:false}},
            {"followRequest.reqFollow":true},
            {"followRequest.followDate" :{$lte:new Date(new Date().getTime()-48*3600*1000)}}
            ]}).toArray();
        console.log("follow those before 2days ago :"+followed.length);
    var index=0;
    var unfollowList=[];

    allResults.forEach(function (acc) {
        var fol =  followed.find(x => x._id === acc.id);
        if(fol) unfollowList.push(fol);
    });
    console.log("find following that i request before 2days ago :"+unfollowList.length);
    for(let unfol of unfollowList){
        throttleUn(function() {
            Client.Relationship.destroy(session, unfol._id).then(function (data) {
                var now = new Date();
                var newvalues = {$set: {"followRequest.$.reqUnFollow": true,"followRequest.$.unfollowDate":new Date()}};
                dbo.collection("account").updateOne({_id: unfol._id,"followRequest.user":users[user].id}, newvalues, function (err, res) {
                    if (err) throw err;
                });
                console.log(index+" unfollow success in "+now+" :"+unfol._id);
            }).catch(error=>{
                var now = new Date();
                var newvalues = {$set: {"followRequest.$.reqUnFollow": false,"followRequest.$.unfollowDate":new Date()}};
                dbo.collection("account").updateOne({_id: unfol._id,"followRequest.user":users[user].id}, newvalues, function (err, res) {
                    if (err) throw err;
                });
                console.log(index + ' unfollow caught in '+now+" :"+unfol._id+"  with error: "+error)
            });
            index++;
        });
    }
}
