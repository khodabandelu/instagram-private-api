// var MongoClient = require('mongodb').MongoClient;
// var url = "mongodb://localhost:27017/instadb";

// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     console.log("Database created!");
//     db.close();
// });

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("instadb");
    // insertMany(db,dbo);
    // updateMany(db,dbo);
    //deleteMany(db, dbo, "accounts")
    findAll(db, dbo, "accounts")

});

function create(db, dbo) {
    dbo.createCollection("customers", function (err, res) {
        if (err) throw err;
        console.log("Collection created!");
        db.close();
    });
}

function insertOne(db, dbo) {
    var myobj = {name: "Company Inc", address: "Highway 37"};
    dbo.collection("customers").insertOne(myobj, function (err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();
    });
}

function insertMany(db, dbo) {
    var array = [
        {name: 'John', address: 'Highway 71'},
        {name: 'Peter', address: 'Lowstreet 4'},
        {name: 'Amy', address: 'Apple st 652'},
        {name: 'Hannah', address: 'Mountain 21'},
        {name: 'Michael', address: 'Valley 345'},
        {name: 'Sandy', address: 'Ocean blvd 2'},
        {name: 'Betty', address: 'Green Grass 1'},
        {name: 'Richard', address: 'Sky st 331'},
        {name: 'Susan', address: 'One way 98'},
        {name: 'Vicky', address: 'Yellow Garden 2'},
        {name: 'Ben', address: 'Park Lane 38'},
        {name: 'William', address: 'Central st 954'},
        {name: 'Chuck', address: 'Main Road 989'},
        {name: 'Viola', address: 'Sideway 1633'}
    ]

    dbo.collection("customers").insertMany(array, function (err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();
    });
}

function findOne(db, dbo) {
    dbo.collection("customers").findOne({}, function (err, result) {
        if (err) throw err;
        console.log(result.name);
        db.close();
    });
}

function findAll(db, dbo, colllection) {
    dbo.collection(colllection).find({}).toArray(function (err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
}

function findSome(db, dbo) {
    dbo.collection("customers").find({}, {_id: 0}).toArray(function (err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
}


function query(db, dbo) {
    var query = {address: "Park Lane 38"};
    dbo.collection("customers").find(query).toArray(function (err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
}

function sort(db, dbo) {
    var mysort = {name: 1};
    dbo.collection("customers").find().sort(mysort).toArray(function (err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
}

function deleteOne(db, dbo) {
    var myquery = {address: 'Mountain 21'};
    dbo.collection("customers").deleteOne(myquery, function (err, obj) {
        if (err) throw err;
        console.log("1 document deleted");
        db.close();
    });
}

function deleteMany(db, dbo, collection, myquery) {
    // var myquery = { address: /^O/ };
    dbo.collection(collection).deleteMany(myquery, function (err, obj) {
        if (err) throw err;
        console.log(obj.result.n + " document(s) deleted");
        db.close();
    });
}

function drop(db, dbo) {
    dbo.collection("customers").drop(function (err, delOK) {
        if (err) throw err;
        if (delOK) console.log("Collection deleted");
        db.close();
    });
}

function updateOne(db, dbo) {
    var myquery = {address: "Valley 345"};
    var newvalues = {$set: {name: "Mickey", address: "Canyon 123"}};
    dbo.collection("customers").updateOne(myquery, newvalues, function (err, res) {
        if (err) throw err;
        console.log("1 document updated");
        db.close();
    });
}


function updateMany(db, dbo) {
    var myquery = {address: /^S/};
    var newvalues = {$set: {name: "Minnie"}};
    dbo.collection("customers").updateMany(myquery, newvalues, function (err, res) {
        if (err) throw err;
        console.log(res.result.nModified + " document(s) updated");
        db.close();
    });
}




